Ansible Role::iTop Webserver
============================

Installs iTop (IT Operational Portal) Webserver layer on Linux servers.

Requirements
------------

Pre-requisites that may not be covered by the role:

- PHP-7.x on Redhat/CEntOS

  ***Remi's RPM repository*** [https://rpms.remirepo.net](https://rpms.remirepo.net) may can be a good starting point.

- SELinux setup

  or `setenforce 0`

- Firewall setup

  Ports may need to be opened for iTop work properly, e.g.:

  - http server ports
  - database server ports

Role Variables
--------------

A description of the settable variables for this role should go here, including any variables that are in defaults/main.yml, vars/main.yml, and any variables that can/should be set via parameters to the role. Any variables that are read from other roles and/or the global scope (ie. hostvars, group vars, etc.) should be mentioned here as well.

Available variables are listed below, along with default values (see **`defaults/main.yml`**):

- apache user/group setup: (e.g. *Debian*: `www-data` || *RedHat*: `apache`)

  ```yaml
  itop_apache_user: 'www-data'
  itop_apache_group: 'www-data'
  ```

- ~~database setup~~ (***TBD***)

  ```yaml
  itop_database_host: localhost
  itop_database_port: 3306
  itop_database_name: itop
  itop_database_username: username
  itop_database_password: password
  ```

Dependencies
------------

A list of other roles hosted on Galaxy should go here, plus any details in regards to parameters that may need to be set for other roles, or variables that are used from other roles.

- roles:
  - Apache 2.x [`geerlingguy.apache (https://galaxy.ansible.com/geerlingguy/apache)`](https://galaxy.ansible.com/geerlingguy/apache)
  - apache virtual host  

    ```yaml
    # iTop vhost via geerlingguy.apache
    apache_listen_ip: "*"
    apache_listen_port: 80
    apache_listen_port_ssl: 443
    apache_vhosts:
      - servername: "itop-webserver.mylab"
        serveralias: "itop-webserver, itop-webserver.example.com"
        documentroot: "/var/www/html/itop/web"
        serveradmin: itop@itop-webserver.example.com
    ```

  - PHP [`geerlingguy.php (https://galaxy.ansible.com/geerlingguy/php)`](https://galaxy.ansible.com/geerlingguy/php)
  - apache virtual host  

    ```yaml
    # iTop PHP bootstrap via geerlingguy.php
    php_upload_max_filesize: "64M"
    php_post_max_size: "128M"
    php_packages:
      - php
      - php-fpm
    ```

Installation samples
--------------------

- via `requirements.yml` file  
    ```yaml
    # sample requirements.yml file with the source from GitLab
    - src: https://gitlab.com/adrianovieira/ansible-role-itop_webserver.git
      scm: git
      name: adrianovieira.itop_webserver
    ```

- ~~or via *Galaxy*~~ (***TBD***)
  ```bash
  ansible-galaxy install adrianovieira.itop_webserver
  ```

Example Playbook
----------------

Including an example of how to use the role (for instance, with variables passed in `vars/main.yml` file):

- ***playbook.yml***

  ```yaml
  ---
  - hosts: itop-webservers
    vars_files:
      - vars/main.yml
    roles:
      - role: adrianovieira.itop_webserver
  ```

- ***vars/main.yml***

  ```yaml
  ---
  # defaults file for ansible-role-itop

  # apache setup
  itop_apache_user: 'www-data'
  itop_apache_group: 'www-data'

  apache_listen_ip: "*"
  apache_listen_port: 80
  apache_listen_port_ssl: 443
  apache_vhosts:
    - servername: "itop-webserver.mylab"
      serveralias: "itop-webserver, itop-webserver.example.com"
      documentroot: "/var/www/html/itop/web"
      serveradmin: itop@itop-webserver.example.com

  # PHP setup
  php_upload_max_filesize: "64M"
  php_post_max_size: "128M"
  php_packages:
  - php
  - php-fpm

  # iTop setup
  itop_release_repository: 'github' # 'sourceforge' || 'github'
  itop_release_version: 'develop'
  itop_rootdir: '/var/www/html/itop'

  # Database setup
  itop_database_host: localhost
  itop_database_port: 3306
  itop_database_name: itop
  itop_database_username: username
  itop_database_password: password
  ```

License
-------

Apache 2.0

Author Information
------------------

An optional section for the role authors to include contact information, or a website (HTML is not allowed).
